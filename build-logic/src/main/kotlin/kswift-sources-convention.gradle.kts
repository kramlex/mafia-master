import org.jetbrains.kotlin.gradle.plugin.mpp.KotlinNativeTarget
import org.jetbrains.kotlin.gradle.plugin.mpp.NativeBuildType
import org.jetbrains.kotlin.gradle.plugin.mpp.apple.XCFramework
import org.jetbrains.kotlin.konan.target.Family
import org.jetbrains.kotlin.konan.target.KonanTarget

plugins {
    id("multiplatform-library-convention")
    id("dev.icerock.moko.kswift")
}

kotlin.targets.withType<KotlinNativeTarget>().configureEach {
    binaries.withType<org.jetbrains.kotlin.gradle.plugin.mpp.Framework>().configureEach {
        linkTask.doLast {
            val from = File(outputDirectory, "${baseName}Swift")
            val to = File(rootDir, "MafiaMaster/MafiaMaster/kswift")
            from.copyRecursively(to, overwrite = true)
        }
    }
}
