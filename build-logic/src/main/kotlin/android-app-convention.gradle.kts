/*
 * Copyright 2021 IceRock MAG Inc. Use of this source code is governed by the Apache 2.0 license.
 */

plugins {
    id("base-convention")
    id("com.android.application")
    id("android-base-convention")
    id("kotlin-android")
}

android {
    defaultConfig {
        multiDexEnabled = true
    }

    dexOptions {
        javaMaxHeapSize = "2g"
    }
}
