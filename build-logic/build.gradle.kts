plugins {
    `kotlin-dsl`
}

repositories {
    mavenCentral()
    google()

    gradlePluginPortal()
}

dependencies {
    api("dev.icerock:mobile-multiplatform:0.14.1")
    api("org.jetbrains.kotlin:kotlin-gradle-plugin:1.6.10")
    api("com.android.tools.build:gradle:7.0.4")
    api("io.gitlab.arturbosch.detekt:detekt-gradle-plugin:1.18.1")
    api("dev.icerock.moko:kswift-gradle-plugin:0.5.0")
}
