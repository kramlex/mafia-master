
enableFeaturePreview("VERSION_CATALOGS")
enableFeaturePreview("TYPESAFE_PROJECT_ACCESSORS")

dependencyResolutionManagement {
    repositories {
        mavenCentral()
        google()
        gradlePluginPortal()
    }
}

val modules = listOf(
    ":androidApp",
    ":shared:game",
    ":shared:home",
    ":shared:entity",
    ":shared",
)

includeBuild("build-logic")
modules.forEach { include(it) }
