import org.jetbrains.kotlin.gradle.plugin.mpp.apple.XCFramework

plugins {
    id("kswift-sources-convention")
    id("dev.icerock.mobile.multiplatform-resources")
    id("dev.icerock.mobile.multiplatform-network-generator")
    id("kotlinx-serialization")
    id("com.squareup.sqldelight")
}

val mppLibs = listOf(
    libs.multiplatformSettings,
    libs.multiplatformSettingsCoroutine,
    libs.napier,
    libs.mokoResources,
    libs.mokoMvvmCore,
    libs.mokoMvvmLiveData,
    libs.mokoUnits,
    libs.mokoErrors,
)

val mppModules = listOf(
    projects.shared.game,
    projects.shared.entity,
    projects.shared.home
)

val kSwiftModules = listOf(projects.shared) + mppModules

dependencies {
    commonMainImplementation(libs.coroutines)
    commonMainImplementation(libs.kotlinSerialization)
    commonMainImplementation(libs.kotlinxDateTime)
    commonMainImplementation(libs.sqlDelightCoroutine)


    mppModules.forEach { commonMainApi(it) }
    mppLibs.forEach { commonMainApi(it) }

    androidMainImplementation(libs.multidex)
    androidMainImplementation(libs.lifecycleViewModel)
    androidMainImplementation(libs.sqlDelightAndroidDriver)

    iosMainImplementation(libs.sqlDelightNativeDriver)
}

sqldelight {
    database("MafiaGamesDatabase") {
        packageName = "ru.kramlex.mafiahost.library.db"
        sourceFolders = listOf("sqldelight/games")
        version = 0
    }
}

multiplatformResources {
    multiplatformResourcesPackage = "ru.kramlex.mafiamaster"
}

kswift {
    install(dev.icerock.moko.kswift.plugin.feature.SealedToSwiftEnumFeature)
    kSwiftModules.forEach { includeLibrary(it.name) }
}

kotlin {
    val xcf = XCFramework()
    listOf(
        iosX64(),
        iosArm64(),
        iosSimulatorArm64()
    ).forEach {
        it.binaries.framework {
            baseName = "shared"
            xcf.add(this)
            mppLibs.forEach { lib -> export(lib) }
            mppModules.forEach { module -> export(module) }
        }
    }
}

afterEvaluate {
    tasks.filterIsInstance<org.jetbrains.kotlin.gradle.plugin.mpp.apple.XCFrameworkTask>()
        .forEach { xcFrameworkTask ->
            val syncName: String = xcFrameworkTask.name.replace("assemble", "sync")

            tasks.create(syncName, Sync::class) {
                this.group = "xcode"

                this.from(File(xcFrameworkTask.outputDir, xcFrameworkTask.buildType.getName()))
                this.into(File(project.buildDir, "xcode"))

                this.dependsOn(xcFrameworkTask)
            }
        }
}