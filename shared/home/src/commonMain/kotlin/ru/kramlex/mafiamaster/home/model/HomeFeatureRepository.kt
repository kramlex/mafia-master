package ru.kramlex.mafiamaster.home.model

interface HomeFeatureRepository {
    val hasActiveGame: Boolean
    fun createGame(masterName: String)
}