package ru.kramlex.mafiamaster.home.presentation

import dev.icerock.moko.mvvm.viewmodel.ViewModel
import dev.icerock.moko.resources.desc.StringDesc
import dev.icerock.moko.resources.desc.desc
import dev.icerock.moko.utils.weakLambda
import dev.icerock.moko.utils.weakLambda1
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import ru.kramlex.mafiamaster.entity.flows.CFlow
import ru.kramlex.mafiamaster.entity.flows.CStateFlow
import ru.kramlex.mafiamaster.entity.flows.cFlow
import ru.kramlex.mafiamaster.entity.flows.cStateFlow
import ru.kramlex.mafiamaster.home.model.HomeFeatureRepository
import ru.kramlex.mafiamaster.home.units.HomeMenuUnit

class HomeViewModel(
    private val repository: HomeFeatureRepository,
    private val strings: Strings
): ViewModel() {

    private val _actions = MutableSharedFlow<Action>()
    val actions: CFlow<Action> get() = _actions.cFlow()

    private val _menuElements: MutableStateFlow<List<HomeMenuUnit>>
    val menuElements: CStateFlow<List<HomeMenuUnit>> get() = _menuElements.cStateFlow()

    init {
        _menuElements = MutableStateFlow(listOf(createGameUnit()))
        viewModelScope.launch {
            delay(1000)
            if (repository.hasActiveGame) {
                _menuElements.value += continueGameUnit()
            }
        }
    }

    private fun emitAction(action: Action) =
        viewModelScope.launch {
            _actions.emit(action)
        }

    private fun createGameUnit(): HomeMenuUnit =
        HomeMenuUnit.CreateNewGame(
            title = strings.createNewGameTitle,
            onClick = weakLambda(this) {
                emitAction(Action.CreateGame(
                    selectName = weakLambda1(this) { masterName ->
                        if (masterName.isNotEmpty()) {
                            repository.createGame(masterName)
                            emitAction(Action.RouteToGame)
                        } else {
                            emitAction(Action.ShowError(message = "Master name is blank".desc()))
                        }

                    }
                ))
            }
        )

    private fun continueGameUnit(): HomeMenuUnit =
        HomeMenuUnit.ContinueGame(
            title = strings.continueGameTitle,
            onClick = weakLambda(this) {
                emitAction(Action.RouteToGame)
            }
        )

    sealed interface Action {
        data class CreateGame(val selectName: (String) -> Unit): Action
        object RouteToGame: Action
        data class ShowError(val message: StringDesc): Action
    }

    interface Strings {
        val createNewGameTitle: StringDesc
        val continueGameTitle: StringDesc
    }
}