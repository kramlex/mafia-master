package ru.kramlex.mafiamaster.home.di

import ru.kramlex.mafiamaster.home.model.HomeFeatureRepository
import ru.kramlex.mafiamaster.home.presentation.HomeViewModel

class HomeFactory(
    private val repository: HomeFeatureRepository,
    private val strings: Strings
) {

    fun createHomeViewModel() =
        HomeViewModel(
            strings = strings,
            repository = repository
        )

    interface Strings: HomeViewModel.Strings
}