package ru.kramlex.mafiamaster.home.units

import dev.icerock.moko.resources.desc.StringDesc

sealed interface HomeMenuUnit {
    data class CreateNewGame(val title: StringDesc, val onClick: () -> Unit): HomeMenuUnit
    data class ContinueGame(val title: StringDesc, val onClick: () -> Unit): HomeMenuUnit
}