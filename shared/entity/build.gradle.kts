plugins {
    id("multiplatform-library-convention")
}

dependencies {
    commonMainImplementation(libs.coroutines)
    commonMainImplementation(libs.kotlinxDateTime)
}
