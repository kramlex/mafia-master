/*
 * Copyright 2022 LLC Campus.
 */

package ru.kramlex.mafiamaster.entity.flows

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.StateFlow

actual class CFlow<T> actual constructor(private val flow: Flow<T>) : Flow<T> by flow
actual class CStateFlow<T> actual constructor(private val flow: StateFlow<T>) : StateFlow<T> by flow