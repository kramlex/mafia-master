package ru.kramlex.mafiamaster.entity.game

import ru.kramlex.mafiamaster.entity.roles.CommissionerModel

data class CommissionerEntity(
    override val gameId: Int,
    val commissionerId: Int,
    val commissionerPlayer: CommissionerModel,
    val commissionerChecks: List<Int>
): GameIdentifiable
