package ru.kramlex.mafiamaster.entity

import ru.kramlex.mafiamaster.entity.roles.PlayerModel

data class PlayerEntity(
    val index: Int,
    val nickname: String,
    val role: PlayerModel,
    val fouls: Int,
    val isAlive: Boolean,
    val isLeave: Boolean,
    val isRemoved: Boolean
) {
    var inGame: Boolean = isAlive && !isLeave && !isRemoved
}