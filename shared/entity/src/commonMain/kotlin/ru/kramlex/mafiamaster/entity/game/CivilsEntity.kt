package ru.kramlex.mafiamaster.entity.game

import ru.kramlex.mafiamaster.entity.roles.CivilModel

data class CivilsEntity (
    override val gameId: Int,
    val civilsId: Int,
    val civils: List<CivilModel>
): GameIdentifiable