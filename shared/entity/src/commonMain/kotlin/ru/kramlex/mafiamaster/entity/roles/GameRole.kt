package ru.kramlex.mafiamaster.entity.roles

enum class GameRole {
    CIVIL, MAFIA, DON, COMMISSIONER
}