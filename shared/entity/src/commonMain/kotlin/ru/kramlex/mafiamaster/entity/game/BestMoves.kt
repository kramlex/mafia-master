package ru.kramlex.mafiamaster.entity.game

data class BestMoves(
    override val gameId: Int,
    val bestMovesId: Int,
    val move: Move?
): GameIdentifiable {

    val isMade: Boolean = move != null

    fun withNewMove(
        playerIndex: Int,
        playerName: String,
        bestMoves: List<Int>
    ) = BestMoves(
        gameId = gameId,
        bestMovesId = bestMovesId,
        move = Move(playerName, playerIndex, bestMoves)
    )

    data class Move(
        val playerName: String,
        val playerIndex: Int,
        val bestMoves: List<Int>
    )

}
