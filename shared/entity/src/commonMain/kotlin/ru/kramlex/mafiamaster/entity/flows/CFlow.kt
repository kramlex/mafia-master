/*
 * Copyright 2022 LLC Campus.
 */

package ru.kramlex.mafiamaster.entity.flows

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.StateFlow


expect class CFlow<T>(flow: Flow<T>) : Flow<T>

expect class CStateFlow<T>(flow: StateFlow<T>) : StateFlow<T>

fun <T> Flow<T>.cFlow(): CFlow<T> = CFlow(this)
fun <T> StateFlow<T>.cStateFlow(): CStateFlow<T> = CStateFlow(this)