package ru.kramlex.mafiamaster.entity

enum class GameStage {
    DAY, NIGHT, VOTES, PICK, FINISHED
}
