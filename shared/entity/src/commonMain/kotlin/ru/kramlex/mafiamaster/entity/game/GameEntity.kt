package ru.kramlex.mafiamaster.entity.game

import kotlinx.datetime.LocalDateTime

data class GameEntity(
    override val gameId: Int,
    val startGameTime: LocalDateTime?,
    val endGameTime: LocalDateTime?
): GameIdentifiable

