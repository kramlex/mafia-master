package ru.kramlex.mafiamaster.entity.game

import ru.kramlex.mafiamaster.entity.GameStage
import ru.kramlex.mafiamaster.entity.roles.GameRole

data class GameStageParams(
    override val gameId: Int,
    val parametersId: Int,
    val stage: GameStage,
    val gameCircle: Int?,
    val activePlayer: Int?,
    val nightRole: GameRole?,
): GameIdentifiable {

    fun update(
        newStage: GameStage = this.stage,
        newGameCircle: Int? = this.gameCircle,
        newActivePlayer: Int? = this.activePlayer,
        newNightRole: GameRole? = this.nightRole
    ) = GameStageParams(
        gameId = gameId,
        parametersId = parametersId,
        stage = newStage,
        activePlayer = newActivePlayer,
        gameCircle = newGameCircle,
        nightRole = newNightRole
    )

}
