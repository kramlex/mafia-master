package ru.kramlex.mafiamaster.entity.roles

sealed interface MafiaModel: PlayerModel {
    data class Mafia(
        override val playerIndex: Int,
        override val playerName: String
    ): MafiaModel {
        override val role: GameRole
            get() = GameRole.MAFIA
    }

    data class Don(
        override val playerName: String,
        override val playerIndex: Int
    ): MafiaModel {
        override val role: GameRole
            get() = GameRole.DON
    }
}
