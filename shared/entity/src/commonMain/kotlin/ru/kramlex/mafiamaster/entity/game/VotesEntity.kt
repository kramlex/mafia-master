package ru.kramlex.mafiamaster.entity.game

import ru.kramlex.mafiamaster.entity.GameVotes

data class VotesEntity(
    override val gameId: Int,
    val votesId: Int,
    val currentCandidates: List<Int>,
    val currentVotes: List<Int>,
    val gameVotes: GameVotes
): GameIdentifiable