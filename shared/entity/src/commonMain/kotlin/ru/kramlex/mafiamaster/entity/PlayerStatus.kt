package ru.kramlex.mafiamaster.entity

enum class PlayerStatus {
    LIVE, LEAVE, DEAD, DELETED
}