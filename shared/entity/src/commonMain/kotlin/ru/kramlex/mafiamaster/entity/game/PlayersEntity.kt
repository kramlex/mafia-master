package ru.kramlex.mafiamaster.entity.game

import ru.kramlex.mafiamaster.entity.PlayerStatus
import ru.kramlex.mafiamaster.entity.roles.GameRole

data class PlayerEntity(
    val playerName: String,
    val playerIndex: Int,
    val role: GameRole,
    val fouls: Int,
    val status: PlayerStatus
)