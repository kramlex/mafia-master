package ru.kramlex.mafiamaster.entity.game

data class FoulsEntity(
    override val gameId: Int,
    val foulsId: Int,
    val fouls: List<Int>,
): GameIdentifiable
