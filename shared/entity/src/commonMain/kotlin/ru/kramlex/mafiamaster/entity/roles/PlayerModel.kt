package ru.kramlex.mafiamaster.entity.roles

interface PlayerModel{
    val playerName: String
    val playerIndex: Int
    val role: GameRole
}
