package ru.kramlex.mafiamaster.entity.game

import ru.kramlex.mafiamaster.entity.roles.MafiaModel

data class MafiaEntity(
    override val gameId: Int,
    val mafiaId: Int,
    val mafia: List<MafiaModel>,
    val donChecks: List<Int>,
    val fires: List<Int>
): GameIdentifiable