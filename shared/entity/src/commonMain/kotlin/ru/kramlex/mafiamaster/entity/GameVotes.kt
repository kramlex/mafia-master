package ru.kramlex.mafiamaster.entity

data class GameVotes(
    val votes: List<GameVote>
)

data class GameVote(
    // playerNumber - count of votes
    val voting: List<Pair<Int,Int>>,
    val revoting: List<Pair<Int,Int>>
)