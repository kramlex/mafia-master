package ru.kramlex.mafiamaster.entity.game

interface GameIdentifiable {
    val gameId: Int
}