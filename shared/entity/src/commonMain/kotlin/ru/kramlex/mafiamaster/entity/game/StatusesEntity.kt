package ru.kramlex.mafiamaster.entity.game

import ru.kramlex.mafiamaster.entity.PlayerStatus

data class StatusesEntity(
    override val gameId: Int,
    val statusesId: Int,
    val statuses: List<PlayerStatus>
): GameIdentifiable