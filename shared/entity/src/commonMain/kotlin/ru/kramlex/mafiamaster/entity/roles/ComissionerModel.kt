package ru.kramlex.mafiamaster.entity.roles

data class CommissionerModel(
    override val playerName: String,
    override val playerIndex: Int
): PlayerModel {
    override val role: GameRole
        get() = GameRole.COMMISSIONER
}
