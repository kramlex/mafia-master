package ru.kramlex.mafiamaster.game.coordinator

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers

class GameCoordinator(
    repository: GameCoordinatorRepository
) {
    private val coordinatorScope = CoroutineScope(Dispatchers.Main)


    interface GameCoordinatorRepository {
    }
}

sealed interface GameCoordinatorStage {
    object Pick : GameCoordinatorStage
    object Day : GameCoordinatorStage
    object Votes : GameCoordinatorStage
    object Night : GameCoordinatorStage
    object Finish : GameCoordinatorStage
}
