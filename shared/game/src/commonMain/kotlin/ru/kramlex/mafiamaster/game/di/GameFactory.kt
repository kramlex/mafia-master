package ru.kramlex.mafiamaster.game.di

import dev.icerock.moko.errors.handler.ExceptionHandler
import ru.kramlex.mafiamaster.game.coordinator.GameCoordinator
import ru.kramlex.mafiamaster.game.models.GameFeatureRepository

class GameFactory(
    private val createExceptionHandler: () -> ExceptionHandler,
    private val repository: GameFeatureRepository,
    private val strings: Strings
) {

    fun createGameCoordinator() = GameCoordinator(repository)

    interface Strings
}