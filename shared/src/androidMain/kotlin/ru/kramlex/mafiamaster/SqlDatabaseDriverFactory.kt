package ru.kramlex.mafiamaster

import android.content.Context
import com.squareup.sqldelight.android.AndroidSqliteDriver
import com.squareup.sqldelight.db.SqlDriver
import ru.kramlex.mafiahost.library.db.MafiaGamesDatabase

actual class SqlDatabaseDriverFactory(
    private val context: Context
) : DatabaseDriverFactory {
    override fun createMerchantsDatabaseDriver(): SqlDriver {
        return AndroidSqliteDriver(
            schema = MafiaGamesDatabase.Schema,
            context = context,
            name = MAFIA_HOST_DATABASE_NAME
        )
    }
}
