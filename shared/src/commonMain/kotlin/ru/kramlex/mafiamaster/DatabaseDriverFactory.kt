package ru.kramlex.mafiamaster

import com.squareup.sqldelight.db.SqlDriver

interface DatabaseDriverFactory {
    fun createMerchantsDatabaseDriver(): SqlDriver
}
