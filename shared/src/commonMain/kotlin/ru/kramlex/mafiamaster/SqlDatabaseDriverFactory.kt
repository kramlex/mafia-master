package ru.kramlex.mafiamaster

import ru.kramlex.mafiamaster.DatabaseDriverFactory

internal const val MAFIA_HOST_DATABASE_NAME = "mafia-host.db"

expect class SqlDatabaseDriverFactory : DatabaseDriverFactory
