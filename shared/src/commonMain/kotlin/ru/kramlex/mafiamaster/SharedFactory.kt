package ru.kramlex.mafiamaster

import com.russhwolf.settings.ExperimentalSettingsApi
import com.russhwolf.settings.ObservableSettings
import dev.icerock.moko.errors.handler.ExceptionHandler
import dev.icerock.moko.errors.mappers.ExceptionMappersStorage
import dev.icerock.moko.errors.presenters.SnackBarDuration
import dev.icerock.moko.errors.presenters.SnackBarErrorPresenter
import io.github.aakira.napier.Antilog
import io.github.aakira.napier.Napier
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import ru.kramlex.mafiamaster.game.di.GameFactory
import ru.kramlex.mafiamaster.database.GamesDao
import ru.kramlex.mafiahost.library.db.MafiaGamesDatabase
import ru.kramlex.mafiamaster.factoryInit.GameFactory
import ru.kramlex.mafiamaster.factoryInit.HomeFactory
import ru.kramlex.mafiamaster.home.di.HomeFactory
import ru.kramlex.mafiamaster.repositories.GameRepository
import ru.kramlex.mafiamaster.repositories.HomeRepository

@ExperimentalSettingsApi
class SharedFactory(
    settings: ObservableSettings,
    antilog: Antilog?,
    databaseDriverFactory: DatabaseDriverFactory
) {

    private val scope = CoroutineScope(Dispatchers.Main)

    private val keyValueStorage: KeyValueStorage by lazy { KeyValueStorage(settings) }

    private val mafiaGamesDatabase = MafiaGamesDatabase(
        databaseDriverFactory.createMerchantsDatabaseDriver(),
        CivilsTableAdapter = GamesDao.civilsTableAdapter,
        CommissionerTableAdapter = GamesDao.commissionerTableAdapter,
        FoulsTableAdapter = GamesDao.foulsTableAdapter,
        GameTableAdapter = GamesDao.gameTableAdapter,
        MafiaTableAdapter = GamesDao.mafiaTableAdapter,
        PlayersStatusTableAdapter = GamesDao.playersStatusTableAdapter,
        VotesTableAdapter = GamesDao.votesTableAdapter,
        GameStageParametersAdapter = GamesDao.gameStageParametersAdapter,
        BestMoveTableAdapter = GamesDao.bestMoveTableAdapter
    )

    private val gamesDao: GamesDao by lazy {
        GamesDao(mafiaGamesDatabase)
    }

    private val gameRepository: GameRepository by lazy {
        GameRepository(
            keyValueStorage = keyValueStorage,
            gamesDao = gamesDao,
            coroutineScope = scope
        )
    }

    private val homeRepository: HomeRepository by lazy {
        HomeRepository(
            keyValueStorage = keyValueStorage,
            gamesDao = gamesDao
        )
    }

    val gameFactory: GameFactory by lazy {
        GameFactory(
            createExceptionHandler = ::createExceptionHandler,
            repository = gameRepository
        )
    }

    val homeFactory: HomeFactory by lazy {
        HomeFactory(
            repository = homeRepository
        )
    }

    init {
        antilog?.also { Napier.base(it) }
    }

    private fun createExceptionHandler(): ExceptionHandler = ExceptionHandler(
        exceptionMapper = ExceptionMappersStorage.throwableMapper(),
        errorPresenter = SnackBarErrorPresenter(duration = SnackBarDuration.LONG),
        onCatch = { error ->
            Napier.e(message = "error caught", throwable = error)
        }
    )
}
