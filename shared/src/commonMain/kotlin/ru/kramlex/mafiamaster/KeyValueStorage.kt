package ru.kramlex.mafiamaster

import com.russhwolf.settings.ExperimentalSettingsApi
import com.russhwolf.settings.ObservableSettings
import com.russhwolf.settings.boolean
import com.russhwolf.settings.coroutines.getBooleanFlow
import com.russhwolf.settings.coroutines.getIntOrNullFlow
import com.russhwolf.settings.nullableInt
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow

@OptIn(
    ExperimentalSettingsApi::class,
    ExperimentalCoroutinesApi::class)
internal class KeyValueStorage(
    settings: ObservableSettings
) {
    val isFirstLaunchFlow: Flow<Boolean> =
        settings.getBooleanFlow(
            key = isFirstLaunchKey,
            defaultValue = isFirstLaunchDefaultValue
        )
    val isFirstLaunch by settings.boolean(isFirstLaunchKey, isFirstLaunchDefaultValue)

    val activeGameIdFlow: Flow<Int?> =
        settings.getIntOrNullFlow(activeGameIdKey)
    var activeGameId by settings.nullableInt(activeGameIdKey)


    companion object {
        const val isFirstLaunchKey = "first_launch"
        const val activeGameIdKey = "active_game_id"

        const val isFirstLaunchDefaultValue = false
    }
}