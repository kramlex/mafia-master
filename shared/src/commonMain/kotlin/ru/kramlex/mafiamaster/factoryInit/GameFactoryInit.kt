package ru.kramlex.mafiamaster.factoryInit

import dev.icerock.moko.errors.handler.ExceptionHandler
import ru.kramlex.mafiamaster.game.di.GameFactory
import ru.kramlex.mafiamaster.game.models.GameFeatureRepository

internal fun GameFactory(
    createExceptionHandler: () -> ExceptionHandler,
    repository: GameFeatureRepository
) = GameFactory(
    createExceptionHandler = createExceptionHandler,
    repository = repository,
    strings = object : GameFactory.Strings {

    }
)