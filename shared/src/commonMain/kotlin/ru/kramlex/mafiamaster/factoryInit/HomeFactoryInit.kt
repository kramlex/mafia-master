package ru.kramlex.mafiamaster.factoryInit

import dev.icerock.moko.resources.desc.StringDesc
import dev.icerock.moko.resources.desc.desc
import ru.kramlex.mafiamaster.home.model.HomeFeatureRepository
import ru.kramlex.mafiamaster.home.di.HomeFactory

internal fun HomeFactory(repository: HomeFeatureRepository) =
    HomeFactory(
        repository = repository,
        strings = object : HomeFactory.Strings {
            override val createNewGameTitle: StringDesc
                get() = "Начать игру".desc()
            override val continueGameTitle: StringDesc
                get() = "Продолжить игру".desc()
        }
    )