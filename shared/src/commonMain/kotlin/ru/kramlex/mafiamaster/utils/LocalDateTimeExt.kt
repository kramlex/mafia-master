package ru.kramlex.mafiamaster.utils

import kotlinx.datetime.LocalDateTime

expect val LocalDateTime.Companion.current: LocalDateTime