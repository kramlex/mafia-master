package ru.kramlex.mafiamaster.utils

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.stateIn

internal interface StateableRepository {

    val coroutineScope: CoroutineScope

    fun <T> Flow<T>.state(initialValue: T): StateFlow<T> =
        this.stateIn(coroutineScope, SharingStarted.Eagerly, initialValue)

    fun <T> Flow<T>.state(): StateFlow<T?> =
        this.stateIn(coroutineScope, SharingStarted.Eagerly, null)

}