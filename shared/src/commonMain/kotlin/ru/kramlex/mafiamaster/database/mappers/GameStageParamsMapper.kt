package ru.kramlex.mafiamaster.database.mappers

import ru.kramlex.mafiahost.library.db.GameStageParameters
import ru.kramlex.mafiamaster.entity.game.GameStageParams

internal fun GameStageParameters.toFeature(): GameStageParams =
    GameStageParams(
        gameId = gameId,
        parametersId = id,
        stage = stage,
        gameCircle = gameCircle,
        activePlayer = activePlayer,
        nightRole = nightRole
    )