package ru.kramlex.mafiamaster.database.adapters

import com.squareup.sqldelight.ColumnAdapter
import ru.kramlex.mafiamaster.entity.roles.CivilModel

internal class ListOfCivilModelAdapter: ColumnAdapter<List<CivilModel>, String> {
    override fun decode(databaseValue: String) =
        if (databaseValue.isEmpty()) {
            listOf()
        } else {
            databaseValue.split(";").map { it.toCivilModel() }
        }
    override fun encode(value: List<CivilModel>) = value.joinToString(separator = ";") { model ->
        "[" + listOf(model.playerIndex, model.playerName).joinToString(infoSeparator) + "]"
    }

    private fun String.toCivilModel(): CivilModel {
        val infoString = this.removeSurrounding(prefix = "[", suffix = "]")
        val splitString = infoString.split(infoSeparator)
        return CivilModel(
            playerIndex = splitString[0].toInt(),
            playerName = splitString[1]
        )
    }
    companion object {
        const val infoSeparator = "|#|"
        val sharedAdapter = ListOfCivilModelAdapter()
    }
}