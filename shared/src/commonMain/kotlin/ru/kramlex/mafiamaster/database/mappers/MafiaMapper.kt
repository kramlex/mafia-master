package ru.kramlex.mafiamaster.database.mappers

import ru.kramlex.mafiahost.library.db.MafiaTable
import ru.kramlex.mafiamaster.entity.game.MafiaEntity

internal fun MafiaTable.toFeature() =
    MafiaEntity(
        gameId = gameId,
        mafiaId = id,
        mafia = info,
        donChecks = donChecks,
        fires = fires
    )