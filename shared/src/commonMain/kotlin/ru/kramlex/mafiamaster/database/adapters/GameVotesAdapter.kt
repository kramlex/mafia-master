package ru.kramlex.mafiamaster.database.adapters

import com.squareup.sqldelight.ColumnAdapter
import ru.kramlex.mafiamaster.entity.GameVotes
import ru.kramlex.mafiamaster.entity.GameVote

internal class GameVotesAdapter: ColumnAdapter<GameVotes, String> {
    override fun decode(databaseValue: String): GameVotes {
        val votesStrings = databaseValue.split(gameVotesSeparator)

        return GameVotes(
            votes = votesStrings.map { GameVote(voteSting = it)}
        )
    }

    override fun encode(value: GameVotes) : String {
        return value.votes.joinToString(gameVotesSeparator) { vote ->
            val votingString = vote.voting.toStringValue().surround(votingBrackets)
            val revotingString = vote.revoting.toStringValue().surround(revotingBrackets)
            listOf(votingString, revotingString).joinToString(revotingSeparator)
        }
    }

    private fun List<Pair<Int,Int>>.toStringValue(): String {
        return this.joinToString(commaSeparator) {
            listOf(
                it.first.toString(),
                it.second.toString()
            ).joinToString(commaSeparator).surround(pairBrackets)
        }
    }

    private fun String.surround(bracketsPair: Pair<String,String>) =
        bracketsPair.first + this + bracketsPair.second

    private fun GameVote(voteSting: String) : GameVote {
        val splitStrings = voteSting.split(revotingSeparator)
        val votingString = splitStrings[0]
            .removeSurrounding(
                prefix = votingBrackets.first,
                suffix = votingBrackets.second
            )
        val revotingString = splitStrings[1]
            .removeSurrounding(
                prefix = revotingBrackets.first,
                suffix = revotingBrackets.second
            )
        return GameVote(
            voting = votingString.toPairList(),
            revoting = revotingString.toPairList()
        )
    }

    private fun String.toPairList(): List<Pair<Int,Int>> =
        this.split(commaSeparator).map { it.toPair() }

    private fun String.toPair(): Pair<Int,Int> {
        val splitString = this
            .removeSurrounding(prefix = pairBrackets.first, suffix = pairBrackets.second)
            .split(commaSeparator)
        return Pair(splitString.first().toInt(), splitString.last().toInt())
    }

    companion object {
        val sharedAdapter = GameVotesAdapter()

        private const val revotingSeparator = "#"
        private const val commaSeparator = ","
        private const val gameVotesSeparator = "|"

        private val votingBrackets: Pair<String,String> = Pair("(", ")")
        private val revotingBrackets: Pair<String, String> = Pair("{", "}")
        private val pairBrackets: Pair<String, String> = Pair("[", "]")
    }
}