package ru.kramlex.mafiamaster.database.mappers

import ru.kramlex.mafiahost.library.db.CivilsTable
import ru.kramlex.mafiamaster.entity.game.CivilsEntity

internal fun CivilsTable.toFeature() = CivilsEntity(
    gameId = gameId,
    civilsId = id,
    civils = civils
)