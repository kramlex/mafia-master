package ru.kramlex.mafiamaster.database.mappers

import ru.kramlex.mafiahost.library.db.FoulsTable
import ru.kramlex.mafiamaster.entity.game.FoulsEntity

internal fun FoulsTable.toFeature() =
    FoulsEntity(
        gameId = gameId,
        foulsId = id,
        fouls = fouls
    )