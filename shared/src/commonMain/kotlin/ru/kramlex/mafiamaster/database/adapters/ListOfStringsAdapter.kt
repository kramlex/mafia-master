package ru.kramlex.mafiamaster.database.adapters

import com.squareup.sqldelight.ColumnAdapter

internal class ListOfStringsAdapter: ColumnAdapter<List<String>, String> {
    override fun decode(databaseValue: String) =
        if (databaseValue.isEmpty()) {
            listOf()
        } else {
            databaseValue.split("|#|#|").map { it }
        }
    override fun encode(value: List<String>) = value.joinToString(separator = "|#|#|")

    companion object {
        val sharedAdapter = ListOfStringsAdapter()
    }
}
