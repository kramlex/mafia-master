package ru.kramlex.mafiamaster.database.mappers

import ru.kramlex.mafiahost.library.db.PlayersStatusTable
import ru.kramlex.mafiamaster.entity.game.StatusesEntity

internal fun PlayersStatusTable.toFeature() =
    StatusesEntity(
        gameId = gameId,
        statuses = playersStatuses,
        statusesId = id
    )