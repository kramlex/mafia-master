package ru.kramlex.mafiamaster.database.mappers

import ru.kramlex.mafiahost.library.db.BestMoveTable
import ru.kramlex.mafiamaster.entity.game.BestMoves

internal fun BestMoveTable.toFeature(): BestMoves {
    val move: BestMoves.Move? = if (playerIndex == null ||
        playerName.isNullOrEmpty() ||
        bestMove.isNullOrEmpty()
    ) { null } else {
        BestMoves.Move(
            playerName = playerName,
            playerIndex = playerIndex,
            bestMoves = bestMove
        )
    }
    return BestMoves(
        gameId = gameId,
        bestMovesId = id,
        move = move
    )
}
