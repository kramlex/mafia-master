package ru.kramlex.mafiamaster.database.mappers

import ru.kramlex.mafiahost.library.db.CommissionerTable
import ru.kramlex.mafiamaster.entity.game.CommissionerEntity
import ru.kramlex.mafiamaster.entity.roles.CommissionerModel

internal fun CommissionerTable.toFeature() =
    CommissionerEntity(
        gameId = gameId,
        commissionerId = id,
        commissionerPlayer = CommissionerModel(
            playerName = playerName,
            playerIndex = playerIndex
        ),
        commissionerChecks = commissionerChecks
    )