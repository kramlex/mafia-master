package ru.kramlex.mafiamaster.database

import com.squareup.sqldelight.EnumColumnAdapter
import com.squareup.sqldelight.runtime.coroutines.asFlow
import com.squareup.sqldelight.runtime.coroutines.mapToOneNotNull
import com.squareup.sqldelight.runtime.coroutines.mapToOneOrNull
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.mapLatest
import kotlinx.datetime.LocalDateTime
import ru.kramlex.mafiamaster.database.adapters.GameVotesAdapter
import ru.kramlex.mafiahost.library.db.BestMoveTable
import ru.kramlex.mafiahost.library.db.CivilsTable
import ru.kramlex.mafiahost.library.db.CommissionerTable
import ru.kramlex.mafiahost.library.db.FoulsTable
import ru.kramlex.mafiahost.library.db.GameStageParameters
import ru.kramlex.mafiahost.library.db.GameTable
import ru.kramlex.mafiahost.library.db.MafiaGamesDatabase
import ru.kramlex.mafiahost.library.db.MafiaTable
import ru.kramlex.mafiahost.library.db.PlayersStatusTable
import ru.kramlex.mafiahost.library.db.VotesTable
import ru.kramlex.mafiamaster.entity.GameStage
import ru.kramlex.mafiamaster.entity.PlayerStatus
import ru.kramlex.mafiamaster.entity.game.BestMoves
import ru.kramlex.mafiamaster.entity.game.CivilsEntity
import ru.kramlex.mafiamaster.entity.game.CommissionerEntity
import ru.kramlex.mafiamaster.entity.game.FoulsEntity
import ru.kramlex.mafiamaster.entity.game.GameEntity
import ru.kramlex.mafiamaster.entity.game.GameStageParams
import ru.kramlex.mafiamaster.entity.game.MafiaEntity
import ru.kramlex.mafiamaster.entity.game.StatusesEntity
import ru.kramlex.mafiamaster.entity.game.VotesEntity
import ru.kramlex.mafiamaster.entity.roles.CivilModel
import ru.kramlex.mafiamaster.entity.roles.CommissionerModel
import ru.kramlex.mafiamaster.entity.roles.GameRole
import ru.kramlex.mafiamaster.entity.roles.MafiaModel
import ru.kramlex.mafiamaster.database.adapters.ListOfCivilModelAdapter
import ru.kramlex.mafiamaster.database.adapters.ListOfIntAdapter
import ru.kramlex.mafiamaster.database.adapters.ListOfMafiaModelAdapter
import ru.kramlex.mafiamaster.database.adapters.ListOfStatusesAdapter
import ru.kramlex.mafiamaster.database.adapters.LocalDateTimeAdapter
import ru.kramlex.mafiamaster.database.mappers.toFeature
import ru.kramlex.mafiamaster.utils.current

@OptIn(ExperimentalCoroutinesApi::class)
internal class GamesDao(database: MafiaGamesDatabase) {
    private val civilsTableQueries = database.civilsTableQueries
    private val commissionerTableQueries = database.commissionerTableQueries
    private val foulsTableQueries = database.foulsTableQueries
    private val gameTableQueries = database.gameTableQueries
    private val mafiaTableQueries = database.mafiaTableQueries
    private val playersStatusTableQueries = database.playersStatusTableQueries
    private val votesTableQueries = database.votesTableQueries
    private val bestMoveQueries = database.bestMoveQueries
    private val gameStageParametersQueries = database.gameStageParametersQueries

    // GameTableQueries

    fun getGameFlowById(gameId: Int): Flow<GameEntity?> =
        gameTableQueries.getGameById(id = gameId)
            .asFlow()
            .mapToOneOrNull()
            .mapLatest { it?.toFeature() }

    fun createGame(masterName: String): Int =
        gameTableQueries.transactionWithResult {
            gameTableQueries.createGameWithStartTime(
                masterName = masterName,
                startGameTime = LocalDateTime.current
            )
            gameTableQueries.selectLastInsertedRowId().executeAsOne().toInt()
        }

    // CivilsTableQueries

    fun getCivilsFlowByGameId(gameId: Int): Flow<CivilsEntity?> =
        civilsTableQueries
            .getCivilsByGameId(gameId = gameId)
            .asFlow()
            .mapToOneOrNull()
            .mapLatest { it?.toFeature() }

    fun createCivils(gameId: Int, civils: List<CivilModel>): Int =
        civilsTableQueries.transactionWithResult {
            civilsTableQueries.createCivilsByGameId(gameId, civils)
            civilsTableQueries.selectLastInsertedRowId().executeAsOne().toInt()
        }

    // CommissionerTableQueries

    fun getCommissionerFlowByGameId(gameId: Int): Flow<CommissionerEntity?> =
        commissionerTableQueries
            .getCommisionerByGameId(gameId = gameId)
            .asFlow()
            .mapToOneOrNull()
            .mapLatest { it?.toFeature() }

    fun createCommissioner(gameId: Int, commissionerModel: CommissionerModel): Int =
        commissionerTableQueries.transactionWithResult {
            commissionerTableQueries.createCommisisonerByGameId(
                gameId = gameId,
                playerIndex = commissionerModel.playerIndex,
                playerName = commissionerModel.playerName
            )
            commissionerTableQueries.selectLastInsertedRowId().executeAsOne().toInt()
        }

    fun updateCommissionerChecks(newCommissioner: CommissionerEntity) =
        commissionerTableQueries.updateCommissionerChecksById(
            commissionerId = newCommissioner.commissionerId,
            newCommissionerChecks = newCommissioner.commissionerChecks
        )

    // FoulsTableQueries

    fun createFouls(gameId: Int): Int =
        foulsTableQueries.transactionWithResult {
            foulsTableQueries.createFoulsByGameId(gameId)
            foulsTableQueries.selectLastInsertedRowId().executeAsOne().toInt()
        }

    fun getFoulsFlowByGameId(gameId: Int): Flow<FoulsEntity> =
        foulsTableQueries
            .getFoulsByGameId(gameId)
            .asFlow()
            .mapToOneNotNull()
            .mapLatest { it.toFeature() }

    fun updateFouls(newFouls: FoulsEntity) =
        foulsTableQueries
            .updateFoulsById(
                foulsId = newFouls.foulsId,
                newFouls = newFouls.fouls
            )

    // MafiaTableQueries

    fun createMafia(gameId: Int, mafia: List<MafiaModel>): Int =
        mafiaTableQueries.transactionWithResult {
            mafiaTableQueries.createMafiaByGameId(gameId = gameId, info = mafia)
            mafiaTableQueries.selectLastInsertedRowId().executeAsOne().toInt()
        }

    fun getMafiaFlowByGameId(gameId: Int): Flow<MafiaEntity?> =
        mafiaTableQueries
            .getMafiaByGameId(gameId)
            .asFlow()
            .mapToOneOrNull()
            .mapLatest { it?.toFeature() }

    fun updateMafiaDonChecks(mafiaWithNewDonChecks: MafiaEntity) =
        mafiaTableQueries.updateDonChecksInGameById(
            mafiaId = mafiaWithNewDonChecks.mafiaId,
            newDonChecks = mafiaWithNewDonChecks.donChecks
        )

    fun updateMafiaFires(mafiaWithNewFires: MafiaEntity) =
        mafiaTableQueries.updateFiresInGameById(
            mafiaId = mafiaWithNewFires.mafiaId,
            newFires = mafiaWithNewFires.fires
        )

    // PlayersStatusTableQueries

    fun createStatuses(gameId: Int, statuses: List<PlayerStatus>): Int =
        playersStatusTableQueries.transactionWithResult {
            playersStatusTableQueries.createMafiaByGameId(
                gameId = gameId,
                playersStatuses = statuses
            )
            playersStatusTableQueries.selectLastInsertedRowId().executeAsOne().toInt()
        }

    fun getStatusesFlowByGameId(gameId: Int): Flow<StatusesEntity> =
        playersStatusTableQueries
            .getMafiaByGameId(gameId)
            .asFlow()
            .mapToOneNotNull()
            .mapLatest { it.toFeature() }

    fun updateStatuses(newStatuses: StatusesEntity) =
        playersStatusTableQueries.updatePlayerStatusesInGameById(
            playerStatusesId = newStatuses.statusesId,
            newPlayersStatuses = newStatuses.statuses
        )

    // VotesTableQueries

    fun createVotes(gameId: Int): Int =
        votesTableQueries.transactionWithResult {
            votesTableQueries.createVotesByGameId(gameId)
            votesTableQueries.selectLastInsertedRowId().executeAsOne().toInt()
        }

    fun getVotesFlowByGameId(gameId: Int) =
        votesTableQueries
            .getVotesByGameId(gameId)
            .asFlow()
            .mapToOneNotNull()
            .mapLatest { it.toFeature() }

    fun updateVotes(new: VotesEntity) =
        votesTableQueries
            .updateVotesById(
                votesId = new.votesId,
                newCurrentVotes = new.currentVotes,
                newGameVotes = new.gameVotes,
                newCurrentCandidates = new.currentCandidates
            )

    // BestMoveQueries

    fun getBestMovesFlowByGameId(gameId: Int): Flow<BestMoves> =
        bestMoveQueries.getBestMoveByGameId(gameId)
            .asFlow()
            .mapToOneNotNull()
            .mapLatest { it.toFeature() }

    fun createBestMoves(gameId: Int) =
        bestMoveQueries.createBestMoveByGameId(gameId)

    fun updateBestMoves(newBestMove: BestMoves) =
        bestMoveQueries.updateBestMove(
            playerIndex = newBestMove.move?.playerIndex,
            playerName = newBestMove.move?.playerName,
            bestMove = newBestMove.move?.bestMoves,
            bestMoveId = newBestMove.bestMovesId
        )

    // Game Stage

    fun getGameStageParamsFlowByGameId(gameId: Int): Flow<GameStageParams> =
        gameStageParametersQueries.getGameParametersByGameId(gameId)
            .asFlow()
            .mapToOneNotNull()
            .mapLatest { it.toFeature() }

    fun createGameStageParams(
        gameId: Int,
        stage: GameStage = GameStage.PICK,
        gameCircle: Int? = null,
        activePlayer: Int? = null,
        nightRole: GameRole? = null
    ) =
        gameStageParametersQueries.createGameParameters(
            gameId = gameId,
            stage = stage,
            gameCircle = gameCircle,
            activePlayer = activePlayer,
            nightRole = nightRole
        )

    fun updateGameStageParams(
        newGameStageParams: GameStageParams
    ) = gameStageParametersQueries.updateGameStageParamsInGame(
        newActivePlayer = newGameStageParams.activePlayer,
        newGameCircle = newGameStageParams.gameCircle,
        newNightRole = newGameStageParams.nightRole,
        newStage = newGameStageParams.stage,
        parametersId = newGameStageParams.parametersId
    )

    // Companion

    companion object {

        val gameTableAdapter = GameTable.Adapter(
            startGameTimeAdapter = LocalDateTimeAdapter.sharedAdapter,
            endGameTimeAdapter = LocalDateTimeAdapter.sharedAdapter,
        )
        val foulsTableAdapter = FoulsTable.Adapter(
            foulsAdapter = ListOfIntAdapter.sharedAdapter
        )
        val mafiaTableAdapter = MafiaTable.Adapter(
            infoAdapter = ListOfMafiaModelAdapter.sharedAdapter,
            donChecksAdapter = ListOfIntAdapter.sharedAdapter,
            firesAdapter = ListOfIntAdapter.sharedAdapter
        )
        val commissionerTableAdapter = CommissionerTable.Adapter(
            commissionerChecksAdapter = ListOfIntAdapter.sharedAdapter
        )

        val civilsTableAdapter = CivilsTable.Adapter(
            civilsAdapter = ListOfCivilModelAdapter.sharedAdapter
        )

        val playersStatusTableAdapter = PlayersStatusTable.Adapter(
            playersStatusesAdapter = ListOfStatusesAdapter.sharedAdapter
        )
        val votesTableAdapter = VotesTable.Adapter(
            currentCandidatesAdapter = ListOfIntAdapter.sharedAdapter,
            currentVotesAdapter = ListOfIntAdapter.sharedAdapter,
            gameVotesAdapter = GameVotesAdapter.sharedAdapter
        )

        val gameStageParametersAdapter = GameStageParameters.Adapter(
            stageAdapter = EnumColumnAdapter(),
            nightRoleAdapter = EnumColumnAdapter()
        )

        val bestMoveTableAdapter = BestMoveTable.Adapter(
            bestMoveAdapter = ListOfIntAdapter.sharedAdapter
        )

    }
}