package ru.kramlex.mafiamaster.database.mappers

import ru.kramlex.mafiahost.library.db.VotesTable
import ru.kramlex.mafiamaster.entity.game.VotesEntity

internal fun VotesTable.toFeature() =
    VotesEntity(
        gameId = gameId,
        votesId = id,
        currentCandidates = currentCandidates,
        currentVotes = currentVotes,
        gameVotes = gameVotes
    )