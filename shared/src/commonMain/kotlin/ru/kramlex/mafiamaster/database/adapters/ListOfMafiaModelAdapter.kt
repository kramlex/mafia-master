package ru.kramlex.mafiamaster.database.adapters

import com.squareup.sqldelight.ColumnAdapter
import ru.kramlex.mafiamaster.entity.roles.MafiaModel

internal class ListOfMafiaModelAdapter: ColumnAdapter<List<MafiaModel>, String> {
    override fun decode(databaseValue: String) =
        if (databaseValue.isEmpty()) {
            listOf()
        } else {
            databaseValue.split(",").map { it.toMafiaModel() }

        }
    override fun encode(value: List<MafiaModel>): String {
        return value.joinToString(",") { it.toEncodedString() }
    }


    private fun MafiaModel.toEncodedString(): String {
        return when(this) {
            is MafiaModel.Mafia -> mafiaBrackets.first +
                    listOf(this.playerIndex, this.playerName).joinToString(infoSeparator) + mafiaBrackets.second
            is MafiaModel.Don -> donBrackets.first +
                    listOf(this.playerIndex, this.playerName).joinToString(infoSeparator) + donBrackets.second
        }
    }

    private fun String.toMafiaModel(): MafiaModel {
        return if (this.hasMafiaSurround()) {
            val infoString = this.removeMafiaSurround()
            val splitInfo = infoString.split(infoSeparator)
            MafiaModel.Mafia(
                playerIndex = splitInfo[0].toInt(),
                playerName = splitInfo[1]
            )
        } else {
            val infoString = this.removeDonSurround()
            val splitInfo = infoString.split(infoSeparator)
            MafiaModel.Don(
                playerName = splitInfo[1],
                playerIndex = splitInfo[0].toInt()
            )
        }
    }

    private fun String.removeDonSurround(): String =
        this.removeSurrounding(prefix = donBrackets.first, suffix = donBrackets.second)

    private fun String.hasMafiaSurround(): Boolean =
        this.startsWith(mafiaBrackets.first) && this.endsWith(mafiaBrackets.second)
    private fun String.removeMafiaSurround(): String =
        this.removeSurrounding(prefix = donBrackets.first, suffix = donBrackets.second)

    companion object {
        const val infoSeparator = "|#|"
        val donBrackets: Pair<String, String> = Pair("{", "}")
        val mafiaBrackets: Pair<String, String> = Pair("[", "]")
        val sharedAdapter = ListOfMafiaModelAdapter()
    }
}
