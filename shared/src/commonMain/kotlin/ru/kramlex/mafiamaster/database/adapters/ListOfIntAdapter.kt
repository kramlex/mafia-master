package ru.kramlex.mafiamaster.database.adapters

import com.squareup.sqldelight.ColumnAdapter

internal class ListOfIntAdapter: ColumnAdapter<List<Int>, String> {
    override fun decode(databaseValue: String) =
        if (databaseValue.isEmpty()) {
            listOf()
        } else {
            databaseValue.split(",").map { it.toInt() }
        }
    override fun encode(value: List<Int>) = value.joinToString(separator = ",")

    companion object {
        val sharedAdapter = ListOfIntAdapter()
    }
}
