package ru.kramlex.mafiamaster.database.mappers

import ru.kramlex.mafiahost.library.db.GameTable
import ru.kramlex.mafiamaster.entity.game.GameEntity

internal fun GameTable.toFeature() = GameEntity(
    gameId = id,
    startGameTime = startGameTime,
    endGameTime = endGameTime
)
