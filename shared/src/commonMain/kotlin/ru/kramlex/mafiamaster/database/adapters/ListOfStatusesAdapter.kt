package ru.kramlex.mafiamaster.database.adapters

import com.squareup.sqldelight.ColumnAdapter
import ru.kramlex.mafiamaster.entity.PlayerStatus

internal class ListOfStatusesAdapter: ColumnAdapter<List<PlayerStatus>, String> {

    private val enumValues = PlayerStatus.values()
    override fun decode(databaseValue: String) =
        if (databaseValue.isEmpty()) {
            listOf()
        } else {
            databaseValue.split(",").map { value ->
                enumValues.first { it.name == value }
            }
        }
    override fun encode(value: List<PlayerStatus>) = value.map { it.name}.joinToString(separator = ",")

    companion object {
        val sharedAdapter = ListOfStatusesAdapter()
    }
}
