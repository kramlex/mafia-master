package ru.kramlex.mafiamaster.database.adapters

import com.squareup.sqldelight.ColumnAdapter
import kotlinx.datetime.LocalDateTime

internal class LocalDateTimeAdapter : ColumnAdapter<LocalDateTime, String> {
    override fun decode(databaseValue: String): LocalDateTime = LocalDateTime.parse(databaseValue)
    override fun encode(value: LocalDateTime): String = value.toString()

    companion object {
        val sharedAdapter = LocalDateTimeAdapter()
    }
}
