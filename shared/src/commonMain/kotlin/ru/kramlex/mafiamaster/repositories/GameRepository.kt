package ru.kramlex.mafiamaster.repositories

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.combine
import ru.kramlex.mafiamaster.KeyValueStorage
import ru.kramlex.mafiamaster.database.GamesDao
import ru.kramlex.mafiamaster.entity.game.CivilsEntity
import ru.kramlex.mafiamaster.entity.game.CommissionerEntity
import ru.kramlex.mafiamaster.entity.game.FoulsEntity
import ru.kramlex.mafiamaster.entity.game.GameEntity
import ru.kramlex.mafiamaster.entity.game.MafiaEntity
import ru.kramlex.mafiamaster.entity.game.PlayerEntity
import ru.kramlex.mafiamaster.entity.game.StatusesEntity
import ru.kramlex.mafiamaster.entity.game.VotesEntity
import ru.kramlex.mafiamaster.game.models.GameFeatureRepository
import ru.kramlex.mafiamaster.utils.StateableRepository

@OptIn(ExperimentalCoroutinesApi::class)
internal class GameRepository(
    private val keyValueStorage: KeyValueStorage,
    private val gamesDao: GamesDao,
    override val coroutineScope: CoroutineScope
): GameFeatureRepository, StateableRepository {

    private val activeGameId: StateFlow<Int?> =
        keyValueStorage.activeGameIdFlow.state()

    // TABLE'S FLOW

    private fun getGameFlow(id: Int): Flow<GameEntity?> =
        gamesDao.getGameFlowById(id)

    private fun getCivils(gameId: Int): Flow<CivilsEntity?> =
        gamesDao.getCivilsFlowByGameId(gameId)

    private fun getCommissioner(gameId: Int): Flow<CommissionerEntity?> =
        gamesDao.getCommissionerFlowByGameId(gameId)

    private fun getFouls(gameId: Int): Flow<FoulsEntity> =
        gamesDao.getFoulsFlowByGameId(gameId)

    private fun getMafia(gameId: Int): Flow<MafiaEntity?> =
        gamesDao.getMafiaFlowByGameId(gameId)

    private fun getPlayerStatuses(gameId: Int): Flow<StatusesEntity> =
        gamesDao.getStatusesFlowByGameId(gameId)

    private fun getVotes(gameId: Int): Flow<VotesEntity> =
        gamesDao.getVotesFlowByGameId(gameId)




    private fun getPlayers(gameId: Int): Flow<List<PlayerEntity>?> {
        return combine(
            getCivils(gameId),
            getCommissioner(gameId),
            getFouls(gameId),
            getMafia(gameId),
            getPlayerStatuses(gameId),
        ) {  civils, commissioner, fouls, mafia, statuses ->
            if (civils == null || commissioner == null || mafia == null) {
                return@combine null
            }
            val players = (civils.civils + commissioner.commissionerPlayer + mafia.mafia).sortedBy { it.playerIndex }

            if (players.count() != 10 || statuses.statuses.count() != 10 || fouls.fouls.count() != 10) return@combine null

            val playersEntities = statuses.statuses.zip(fouls.fouls).zip(players) { (status, foul), player ->
                PlayerEntity(
                    playerName = player.playerName,
                    playerIndex = player.playerIndex,
                    role = player.role,
                    fouls = foul,
                    status = status
                )
            }

            return@combine playersEntities
        }
    }

}