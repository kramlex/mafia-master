package ru.kramlex.mafiamaster.repositories

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.stateIn
import ru.kramlex.mafiamaster.KeyValueStorage
import ru.kramlex.mafiamaster.database.GamesDao
import ru.kramlex.mafiamaster.entity.game.CivilsEntity
import ru.kramlex.mafiamaster.entity.game.CommissionerEntity
import ru.kramlex.mafiamaster.entity.game.FoulsEntity
import ru.kramlex.mafiamaster.entity.game.GameEntity
import ru.kramlex.mafiamaster.entity.game.MafiaEntity
import ru.kramlex.mafiamaster.entity.game.PlayerEntity
import ru.kramlex.mafiamaster.entity.game.StatusesEntity
import ru.kramlex.mafiamaster.entity.game.VotesEntity
import ru.kramlex.mafiamaster.home.model.HomeFeatureRepository
import ru.kramlex.mafiamaster.utils.StateableRepository

@OptIn(ExperimentalCoroutinesApi::class)
internal class HomeRepository(
    private val keyValueStorage: KeyValueStorage,
    private val gamesDao: GamesDao,
): HomeFeatureRepository {

    private val activeGameId: Int?
        get() = keyValueStorage.activeGameId

    override val hasActiveGame: Boolean
        get() = true

    override fun createGame(masterName: String) {
        keyValueStorage.activeGameId = gamesDao.createGame(masterName)
    }

}