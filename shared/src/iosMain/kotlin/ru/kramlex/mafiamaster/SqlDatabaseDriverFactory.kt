package ru.kramlex.mafiamaster

import com.squareup.sqldelight.db.SqlDriver
import com.squareup.sqldelight.drivers.native.NativeSqliteDriver
import ru.kramlex.mafiahost.library.db.MafiaGamesDatabase

actual class SqlDatabaseDriverFactory : DatabaseDriverFactory {
    override fun createMerchantsDatabaseDriver(): SqlDriver {
        return NativeSqliteDriver(
            MafiaGamesDatabase.Schema,
            MAFIA_HOST_DATABASE_NAME
        )
    }
}
