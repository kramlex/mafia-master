package ru.kramlex.mafiamaster.utils

import kotlinx.datetime.Instant
import kotlinx.datetime.LocalDateTime
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toLocalDateTime
import platform.Foundation.NSDate
import platform.Foundation.timeIntervalSince1970

actual val LocalDateTime.Companion.current: LocalDateTime
    get() = Instant
        .fromEpochMilliseconds(NSDate().timeIntervalSince1970.toLong())
        .toLocalDateTime(TimeZone.currentSystemDefault())