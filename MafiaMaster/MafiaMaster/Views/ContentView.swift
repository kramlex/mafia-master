//
//  ContentView.swift
//  MafiaMaster
//
//  Created by mdubkov on 10.04.2022.
//

import SwiftUI
import UIPilot

enum AppRoute: Equatable {
    case home
    case game
}

struct ContentView: View {
    @StateObject var pilot = UIPilot(initial: AppRoute.home)
    
    var body: some View {
        UIPilotHost(pilot) { route in
            switch route {
            case .home:
                return AnyView(
                    HomeViewBinding()
                )
            case .game:
                return AnyView(
                    Text("Game todo implement's")
                )
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
