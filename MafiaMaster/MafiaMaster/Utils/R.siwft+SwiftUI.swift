//
//  R.siwft+SwiftUI.swift
//  MafiaMaster
//
//  Created by mdubkov on 10.04.2022.
//

import Foundation
import Rswift
import SwiftUI

extension ColorResource {
    var color: Color {
        Color(self.name)
    }
}
