//
//  MafiaMasterApp.swift
//  MafiaMaster
//
//  Created by mdubkov on 10.04.2022.
//

import SwiftUI
import Bagel
import shared

@main
struct MafiaMasterApp: App {
    
    init() {
        let antilog: Antilog?
        #if DEBUG
        Bagel.start()
        antilog = DebugAntilog(defaultTag: "debug")
        #endif
        
        AppComponent.factory = SharedFactory(
            settings: AppleSettings(delegate: UserDefaults.standard),
            antilog: antilog,
            databaseDriverFactory: SqlDatabaseDriverFactory()
        )
    }
    
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
