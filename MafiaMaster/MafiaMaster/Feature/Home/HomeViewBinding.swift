//
//  HomeViewBinding.swift
//  MafiaMaster
//
//  Created by mdubkov on 10.04.2022.
//

import SwiftUI
import shared
import Combine
import UIPilot
import Differ
import AlertToast

private func createViewModel() -> HomeViewModel {
    return AppComponent
        .factory
        .homeFactory
        .createHomeViewModel()
}

struct HomeViewBinding: View {
    @EnvironmentObject var pilot: UIPilot<AppRoute>
    @ObservedObject var viewModel: HomeViewModel = createViewModel()
    @State private var elements: [HomeMenuUnitKs] = []
    @State private var showAlert: Bool = false
    @State private var alertText: String = ""
    @State private var isInput: Bool = false
    @State private var masterName: String = ""
    
    var body: some View {
        ZStack {
            R.color.appBackground.color
                .ignoresSafeArea()
            HomeViewBody(
                menuElements: $elements,
                isInput: $isInput,
                masterName: $masterName
            ).onReceive(viewModel.actionsKs) { action in
                switch action {
                case let .createGame(createGameAction):
                    if !isInput {
                        withAnimation {
                            isInput.toggle()
                        }
                    } else {
                        createGameAction.selectName(masterName)
                    }
                case .routeToGame:
                    pilot.push(.game)
                case let .showError(showErrorAction):
                    alertText = showErrorAction.message.localized()
                    showAlert.toggle()
                }
            }.onReceive(viewModel.menuElementKs) { menuElements in
                withAnimation {
                    elements = menuElements
                }
            }
        }
        .contentShape(Rectangle())
        .onTapGesture(count: 2) {
            withAnimation{
                isInput.toggle()
            }
        }
        .toast(
            isPresenting: $showAlert,
            duration: 2.0,
            tapToDismiss: true,
            alert: {
                AlertToast(displayMode: .hud, type: .regular, title: alertText)
            }
        )
    }
}

extension HomeViewModel {
    var menuElementKs: AnyPublisher<[HomeMenuUnitKs], Never> {
        createPublisher(self.menuElements)
            .map { $0.map { element in HomeMenuUnitKs(element as! HomeMenuUnit) } }
            .eraseToAnyPublisher()
    }
    
    var actionsKs: AnyPublisher<HomeViewModelActionKs, Never> {
        createPublisher(self.actions)
            .map { HomeViewModelActionKs($0) }
            .eraseToAnyPublisher()
    }
}

struct HomeViewBody: View {
    
    @Binding var menuElements: [HomeMenuUnitKs]
    @Binding var isInput: Bool
    @Binding var masterName: String
    
    var body: some View {
        
        VStack {
            ForEach(menuElements) { element in
                switch element {
                case let .createNewGame(unit):
                    ButtonRow(title: unit.title.localized(), style: .primary, onClick: unit.onClick)
                    if isInput {
                        TextFieldRow(text: $masterName, placeholder: "Enter master name")
                    }
                case let .continueGame(unit):
                    ButtonRow(title: unit.title.localized(), style: .secondary, onClick: unit.onClick)
                }
                
            }
        }
    }
}

extension HomeMenuUnitKs: Identifiable, Equatable {
    
    public static func == (lhs: HomeMenuUnitKs, rhs: HomeMenuUnitKs) -> Bool {
        return lhs.id == rhs.id
    }
    
    public var id: Int {
        switch self {
        case .continueGame: return 1
        case .createNewGame: return 0
        }
    }
}




