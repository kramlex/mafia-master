//
//  TextFieldRow.swift
//  MafiaMaster
//
//  Created by mdubkov on 10.04.2022.
//

import SwiftUI
import iTextField

struct TextFieldRow: View {
    
    @Binding var text: String
    let placeholder: String
    
    var body: some View {
        HStack {
            Spacer()
            iTextField(placeholder, text: $text)
                .accentColor(.gray)
                .fontFromUIFont(UIFont(name: "Avenir", size: 15))
                .multilineTextAlignment(.center)
                .keyboardType(.default)
                .returnKeyType(.done)
                .disableAutocorrection(true)
                .autocapitalization(.none)
            
            Spacer()
        }
        .frame(maxWidth: .infinity)
        .padding()
        .transition(.opacity)
    }
}

struct TextFieldRow_Previews: PreviewProvider {
    
    private static var text: String = ""
    
    static var previews: some View {
        TextFieldRow(
            text: Binding(
                get: { text },
                set: { value, transaction in
                    text = value
                }
            ),
            placeholder: "Enter master name"
        )
        .previewLayout(.sizeThatFits)
    }
}
