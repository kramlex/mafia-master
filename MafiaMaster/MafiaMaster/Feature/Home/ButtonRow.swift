//
//  Row.swift
//  MafiaMaster
//
//  Created by mdubkov on 10.04.2022.
//

import SwiftUI

struct ButtonRow: View {

    var title: String
    var style: Style
    var onClick: () -> Void

    var body: some View {
        HStack {
            Spacer()
            HStack {
                Button {
                    onClick()
                } label: {
                    Text(title)
                        .font(.caption)
                }
                .tint(style.buttonTint)
                .buttonStyle(.bordered)
                .buttonBorderShape(.capsule)
                .controlSize(.large)
            }
            
            Spacer()
        }
        .frame(maxWidth: .infinity)
        .padding()
        .transition(.move(edge: .leading))
        
    }
    
    enum Style {
        
        case primary
        case secondary
        
        var buttonTint: Color {
            switch self {
            case .primary: return .green
            case .secondary: return .pink
            }
        }
        
        
    }
}

struct Row_Previews: PreviewProvider {
    static var previews: some View {
        ButtonRow(title: "Create new game", style: .primary, onClick: {})
            .previewLayout(.sizeThatFits)
        ButtonRow(title: "Continue game", style: .secondary, onClick:  {})
            .previewLayout(.sizeThatFits)
    }
}
