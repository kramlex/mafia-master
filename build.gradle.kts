buildscript {
    repositories {
        gradlePluginPortal()
        google()
        mavenCentral()
    }
    dependencies {
        classpath(libs.mokoNetworkGeneratorGradle)
        classpath(libs.mokoResourcesGeneratorGradle)
        classpath(libs.kotlinSerializationGradle)
        classpath(libs.kSwiftGradle)
        classpath(libs.hiltGradle)
        classpath(libs.sqlDelightGradle)
        classpath(":build-logic")
    }
}

allprojects {
    configurations.configureEach {
        resolutionStrategy {
            val coroutines: MinimalExternalModuleDependency = rootProject.libs.coroutines.get()
            val forcedCoroutines: ModuleVersionSelector = org.gradle.api.internal.artifacts.DefaultModuleVersionSelector.newSelector(
                coroutines.module,
                coroutines.versionConstraint.requiredVersion
            )
            force(forcedCoroutines)
        }
    }
}

tasks.register("clean", Delete::class) {
    delete(rootProject.buildDir)
}